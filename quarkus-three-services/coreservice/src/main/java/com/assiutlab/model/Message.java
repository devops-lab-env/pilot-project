package com.assiutlab.model;

import lombok.Data;

public @Data class Message {
    private String message;
}
