package com.assiutlab.client;

import com.assiutlab.factory.RequestSpanIDHeaderFactory;
import com.assiutlab.model.QuarkusModul;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.Set;
import java.util.concurrent.CompletionStage;

@Path("/core")
@RegisterRestClient(configKey = "core-client")
@Singleton
@RegisterClientHeaders
public interface CoreClient {

    @GET
    String getCoreMessage();

    @GET
    CompletionStage<String> getCoreMessageAsync();
    @GET
    @Path("/be")
    String getCoreBeMessage();

    @GET
    @Path("/be/extensions")
    Set<QuarkusModul> getQuarkusModules();
}
