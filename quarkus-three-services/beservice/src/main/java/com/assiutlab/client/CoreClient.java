package com.assiutlab.client;

import com.assiutlab.factory.RequestSpanIDHeaderFactory;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/core")
@RegisterRestClient(configKey = "core-client")
@Singleton
@RegisterClientHeaders
public interface CoreClient {
    @GET
    String getCoreMessage();
}
