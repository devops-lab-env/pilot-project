package com.assiutlab;

import com.assiutlab.client.BeClient;
import com.assiutlab.client.CoreClient;
import com.assiutlab.model.QuarkusModul;
import com.assiutlab.service.GreetingService;
import io.quarkus.logging.Log;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;

@Path("/fe")
public class ExampleResource {

    private static final Logger LOG = Logger.getLogger(ExampleResource.class);
    @Inject
    GreetingService greetingService;

    @RestClient
    CoreClient coreClient;

    @RestClient
    BeClient beClient;

    @GET()
    public String hello(@Context HttpHeaders requHttpHeaders) {
        return greetingService.greeting();
    }

    @GET
    @Path("/core")
    public String getCoreMessage(){
        return coreClient.getCoreMessage()+" through FE SERVICE";
    }
    @GET
    @Path("/core/be")
    public String getCoreBeMessage(){
        return coreClient.getCoreBeMessage()+" By FE Service";
    }
    @GET
    @Path("/core/be/extensions")
    public Set<QuarkusModul> getQuarkusModules(){
        return coreClient.getQuarkusModules();
    }


    @GET
    @Path("/be")
    public String getBeMessage() {
        return beClient.getBeMessage()+" through FE SERVICE";
    }

    @GET
    @Path("/coreAsync")
    public CompletionStage<String> getCoreMessageAsync(){
        return coreClient.getCoreMessageAsync();
    }

}