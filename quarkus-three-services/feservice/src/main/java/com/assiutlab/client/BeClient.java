package com.assiutlab.client;

import com.assiutlab.factory.RequestSpanIDHeaderFactory;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/be")
@RegisterRestClient(configKey = "be-client")
@Singleton
@RegisterClientHeaders
public interface BeClient {
    @GET
    String getBeMessage();
}
