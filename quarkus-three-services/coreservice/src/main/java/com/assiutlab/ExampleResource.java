package com.assiutlab;

import com.assiutlab.client.BeClient;
import com.assiutlab.client.FeClient;
import com.assiutlab.model.QuarkusModul;
import com.assiutlab.service.GreetingService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/core")
public class ExampleResource {

    @Inject
    GreetingService greetingService;

    @RestClient
    BeClient beClient;

    @RestClient
    FeClient feClient;

    @GET
    public String hello() {
        return greetingService.greeting();
    }

    @GET
    @Path("/be")
    public String getBeMessage(){
        return beClient.getBeMessage()+" through CORE SERVICE";
    }

    @GET
    @Path("/fe")
    public String getFeMessage(){
        return feClient.getFeMessage()+" through CORE SERVICE";
    }

    @GET
    @Path("/be/extensions")
    public Set<QuarkusModul> getQuarkusModules(){
        return beClient.getQuarkusModules();
    }

}
