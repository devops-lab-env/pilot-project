package com.assiutlab.model;

import lombok.Data;

public @Data class Message {
    String message;
}
