package com.assiutlab;

import com.assiutlab.client.CoreClient;
import com.assiutlab.client.FeClient;
import com.assiutlab.client.QuarkusAPIClient;
import com.assiutlab.model.QuarkusModul;
import com.assiutlab.service.GreetingService;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/be")
public class ExampleResource {

    @RestClient
    QuarkusAPIClient quarkusAPIClient;

    @RestClient
    FeClient feClient;

    @RestClient
    CoreClient coreClient;

    @Inject
    GreetingService greetingService;

    @GET
    public String hello() {
        return greetingService.greeting();
    }

    @GET
    @Path("/extensions")
    public Set<QuarkusModul> getQuarkusModules(){
        return quarkusAPIClient.getExtensions();
    }

    @GET
    @Path("/fe")
    public String getFeMessage(){
        return feClient.getFeMessage()+" through BE SERVICE";
    }

    @GET
    @Path("/core")
    public String getCoreMessage(){
        return coreClient.getCoreMessage()+" through BE SERVICE";
    }

}