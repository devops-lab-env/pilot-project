package com.assiutlab.factory;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;
import org.jboss.logging.Logger;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RequestSpanIDHeaderFactory implements ClientHeadersFactory {

    private static final Logger LOG = Logger.getLogger(RequestSpanIDHeaderFactory.class);

    private static Optional<Config> config() {
        try {
            return Optional.ofNullable(ConfigProvider.getConfig());
        } catch (NoClassDefFoundError | IllegalStateException | ExceptionInInitializerError var1) {
            return Optional.empty();
        }
    }

    private static Optional<String> getHeadersProperty() {
        Optional<Config> c = config();
        return c.isPresent() ? Optional.ofNullable(((Config)c.get()).getOptionalValue("org.eclipse.microprofile.rest.client.propagateHeaders", String.class).orElse((String) null)) : Optional.empty();
    }

    @Override
    public MultivaluedMap<String, String> update(MultivaluedMap<String, String> incomingHeaders, MultivaluedMap<String, String> clientOutgoingHeaders) {

        for(Map.Entry<String, List<String>> entry: incomingHeaders.entrySet()){
            LOG.info("incomingHeaders - Header Name: "+entry.getKey()+" - Header Value: "+entry.getValue());
        }

        for(Map.Entry<String, List<String>> entry: clientOutgoingHeaders.entrySet()){
            LOG.info("clientOutgoingHeaders - Header Name: "+entry.getKey()+" - Header Value: "+entry.getValue());
        }

        MultivaluedMap<String, String> propagatedHeaders = new MultivaluedHashMap();
        Optional<String> propagateHeaderString = getHeadersProperty();
        if (propagateHeaderString.isPresent()) {
            Arrays.stream(((String)propagateHeaderString.get()).split(",")).forEach((header) -> {
                if (incomingHeaders.containsKey(header)) {
                    propagatedHeaders.put(header, incomingHeaders.get(header));
                }

            });
        }

        for(Map.Entry<String, List<String>> entry: propagatedHeaders.entrySet()){
            LOG.info("propagatedHeaders - Header Name: "+entry.getKey()+" - Header Value: "+entry.getValue());
        }

        return propagatedHeaders;
    }
}
