package com.assiutlab.service;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingService {


    public String greeting(){
        return "Hello from Back-End Service";
    }

}
