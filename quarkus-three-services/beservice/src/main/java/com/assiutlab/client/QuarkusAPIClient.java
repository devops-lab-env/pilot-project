package com.assiutlab.client;

import com.assiutlab.factory.RequestSpanIDHeaderFactory;
import com.assiutlab.model.QuarkusModul;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Set;


@RegisterRestClient(configKey = "api-client")
@RegisterClientHeaders(RequestSpanIDHeaderFactory.class)
public interface QuarkusAPIClient {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Set<QuarkusModul> getExtensions();

}
