package com.assiutlab.factory;

import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;
import org.jboss.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Map;

public class RequestSpanIDHeaderFactory implements ClientHeadersFactory {

    private static final Logger LOG = Logger.getLogger(RequestSpanIDHeaderFactory.class);


    @Override
    public MultivaluedMap<String, String> update(MultivaluedMap<String, String> incomingHeaders, MultivaluedMap<String, String> clientOutgoingHeaders) {

        for(Map.Entry<String, List<String>> entry: incomingHeaders.entrySet()){
            LOG.info("incomingHeaders - Header Name: "+entry.getKey()+" - Header Value: "+entry.getValue());
        }

        for(Map.Entry<String, List<String>> entry: clientOutgoingHeaders.entrySet()){
            LOG.info("clientOutgoingHeaders - Header Name: "+entry.getKey()+" - Header Value: "+entry.getValue());
        }
        return clientOutgoingHeaders;
    }
}
