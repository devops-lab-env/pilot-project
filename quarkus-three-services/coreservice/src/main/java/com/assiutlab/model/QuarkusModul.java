package com.assiutlab.model;


import lombok.Data;

import java.util.List;

public @Data class QuarkusModul {
    private String id;
    private String shortId;
    private String version;
    private String name;
    private String description;
    private String shortName;
    private String category;
    private List<String> tags;
    private List<String> keywords;
    private boolean providesExampleCode;
    private boolean providesCode;
    private String guide;
    private int order;
    private boolean platform;
    private String bom;

}
