# Pilot Project
This activity aims to understand DevOps concepts by creating a simple microservice architecture in a few minutes.

This project creates a simple microservice architecture on one host machine (running VMs on one host) to simplify, monitor and how its work.

# Architecture Vision
![](images/architecture-vision.png)
## Software Architecture Vision
![](images/software-architecture-vision.png)

# Implementing this Architecture Vision using:
## Java Development Tools:
- Quarkus
- Maven
## OS Concepts:
- Virtualization
- Ubuntu Server 20.O4
## Infrastructure as Code Tools:
- Containers
  - Docker
  - quarkus-container-image-jib - Maven Dependency by Quarkus
- Configuration Management:
  - Ansible
- Container Orchiestration:
  - Kubernetes
  - quarkus-kubernetes - Maven Dependency by Quarkus
- Service Mesh
  - Istio
## Monitor software and infrastructure Tools:
- Infrastructure Monitoring 
  - Kubernetes Dashboard
  - Prometheus
  - Grafana
  - Kiali
- Application Monitoring (Tracing) 
  - Jaeger
- Logs Monitoring
  - Elastic Stack
    - ElasticSearch
    - Kibana
    - Fluentd
## Before you begin
A compatible Linux Host with 16 GB or more and 8 CPUs or more
- [Installed Ubuntu 20.04](https://ubuntu.com/tutorials/install-ubuntu-server#3-boot-from-install-media)
- [Installed Vagrant](https://www.vagrantup.com/docs/installation)
- [Installed VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads)
- [Clone project from GitLab in <local-directory-host>](https://gitlab.com/lab-devops-env/pilot-project)

## Running Kubernetes Cluster VMs
The Kubernetes Cluster has two VMs, one for control-plane node (master-1) and another one for worker node (worker-1).

To create two VMs of Kubernetes Cluster (master-1 & worker-1), run the below command under <local-directory-host>/env/cluster-1/
```shell script
vagrant up
```


## Creating Ansible Management Node
To create Ansible VM, run the below command under <local-directory-host>/env/helper/
```shell script
vagrant up
```
Access to ansible node by vagrant SSH command from host machine.
run the below command under <local-directory-host>/env/helper/
```shell script
vagrant ssh
```
Output:
```console
ubuntu-1@ubuntu-1:~/vagrant-code/lab-env/helper$ vagrant ssh
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-22-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon Aug  1 07:45:02 PM UTC 2022

  System load:  0.0                Processes:             105
  Usage of /:   11.4% of 30.83GB   Users logged in:       0
  Memory usage: 9%                 IPv4 address for eth0: 10.0.2.15
  Swap usage:   0%                 IPv4 address for eth1: 192.168.56.200


This system is built by the Bento project by Chef Software
More information can be found at https://github.com/chef/bento
vagrant@ansible:~$
```

### [Installing ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
```shell script
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.9 get-pip.py --user
python3.9 -m pip install --user ansible
```
You need to add the below lines to your ~/.profile file
```shell script
export PATH="$PATH:~/.local/bin"
export ANSIBLE_CONFIG=/vagrant/cfg/ansible.cfg
```
You should run the below command for the remainder of the session
```shell script
source ~/.profile
```

### Creating Kubernetes Cluster by Ansible Node
Run the below commands on ansible node
```shell script
sudo apt install sshpass
ssh-keyscan -H 192.168.56.111 >> ~/.ssh/known_hosts
ansible-playbook /vagrant/play-book/k8s-master/k8s-master.yaml
ssh-keyscan -H 192.168.56.121 >> ~/.ssh/known_hosts
ansible-playbook /vagrant/play-book/k8s-worker/k8s-worker.yaml
```
### Install Istio Service Mesh with addons (Prometheus, Kiali, Jaeger and Grafana)
Run the below command on ansible node.
```shell script
ansible-playbook /vagrant/play-book/k8s-master/istio.yaml
```
### Expose Istio addons service
You must run the below command to expose port for (Prometheus, Kiali, Jaeger and Grafana)
```shell script
ansible-playbook /vagrant/play-book/k8s-master/expose-istio-addons-ui.yaml
```
Now you can access to istio addons tools from host machine
- Grafana: http://localhost:30002
- Prometheus: http://localhost:30004
- Kiali: http://localhost:32028/kiali
- Jaeger: http://localhost:32028/jaeger
### Install Kubernetes Dashboard from Ansible Node
Run the below command on ansible node.
```shell script
ansible-playbook /vagrant/play-book/k8s-master/k8s-dashboard.yaml
```
You should set install_k8s_dashboard: false in the above play-book to only generate token for accessing to kubernetes dashboard.

You can find token file to access to Kubernetes Dashboard in master-1 node under /vagrant/master-1/token_k8s_dashboar

### Install Elastic Stack (Fluentd, ElasticSearch & Kibana) from ansible node
Run the below command on ansible node.
```shell script
ansible-playbook /vagrant/play-book/k8s-master/efk.yaml
```
Now you can access to Kibana UI from host machine
- Kibana UI: http://localhost:30005

### Install a simple application with three services of Quarkus (FE, Core & BE)
Run the below command on ansible node.
```shell script
ansible-playbook /vagrant/play-book/k8s-master/quarkus-three-services.yaml
```
You can test the application from host machine
- Fe Service: 
  - http://localhost/fe
  - http://localhost/fe/core
  - http://localhost/fe/core/be
  - http://localhost/fe/core/be/extensions
- Core Service:
  - http://localhost/core
  - http://localhost/core/be
  - http://localhost/core/be/extensions
- Be Service:
  - http://localhost/be
  - http://localhost/be/extensions 






